
//Usuario Model

var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var carroSchema = new Schema({

    nombre: {  type: String, required: [true, ' El nombre es requerido']},
    modelo: {  type: String, required: [true, ' El modelo es requerido']},
    ano: {  type: String, required: [true, ' El ano es requerido']},
    descripcao: {  type: String, required: [true, ' El descripcao es requerido']},



    //email: {  type: String, unique:true, required: [true, ' El email es requerido']},
    //password: {  type: String, required: [true, ' El password es requerido']},
    img: {  type: String, required: false },
    role: { type: String, required: true, default: 'USER_ROLE'}

});

// Hay que exportar el usuario para poder usar en otra seccion
module.exports = mongoose.model('Carro', carroSchema);