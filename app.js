//Requires
var express = require('express');
var bodyParser = require('body-parser');
const mongoose = require('mongoose');

//Iniciar variables 
var app = express();

//Cors
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "YOUR-DOMAIN.TLD"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");

    next();
});

mongoose.connect('mongodb://localhost:27017/prueba', (err, resp) => {
    if (err) throw err;
    console.log('Base de datos  puerto 27017: \x1b[32m%s\x1b[0m', 'online');
});

// Body Parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// Importar Rutas
var appRoutes = require('./routes/app');
var usuarioRoutes = require('./routes/usuario');
var carroRoutes = require('./routes/carro');


//Routes
app.use('/usuario', usuarioRoutes);
app.use('/carro', carroRoutes);

app.use('/', appRoutes);


// Escuchar peticiones
app.listen(3000, () => {
    console.log('Express server puerto 3000: \x1b[32m%s\x1b[0m', 'online');
});