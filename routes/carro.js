var express = require('express');

var app = express();

var Carro = require('../models/carro');

app.get('/', ( req, res, next ) => {

 Carro.find({},'nombre modelo ano img role')
 .exec(
 ( err, carros)=>{

    if (err){
        return res.status(500).json({
            ok: false,
            mensaje: 'Error de base de datos',
            errors: err
        });
    }

    return res.status(200).json({
        ok: true,
        carros: carros
    });

 });

});
//=============================//
//Crear nuevo usuario
//=============================//
app.post('/', ( req, res)=>{

var body = req.body;
var carro = new Carro({
    nombre: body.nombre,
    modelo: body.modelo,
    ano: body.ano,
    descripcao: body.descripcao,
    img: body.img,  
    role: body.role
});

carro.save(( err, guardado)=>{

    if (err){
        return res.status(500).json({
            ok: false,
            mensaje: 'Error al crear um modelo de carro',
            errors: err
        });
    }

    return res.status(200).json({
        ok: true,
        gusuario: guardado
    });

});


})


module.exports = app;